import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String huruf = "";
        Integer count1 = 0;
        Integer count2 = 0;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Masukkan huruf: ");
        huruf = keyboard.nextLine();

        for (int i = 0; i<huruf.length(); i++) {
            char c = huruf.charAt(i);
            if (Character.toString(c).equals("x")) {
                count1 += 1;
            } else if (Character.toString(c).equals("o")) {
                count2 += 1;
            } else {
                System.out.println("Karakter tidak valid");
                break;
            }
        }

        Boolean statement = false;
        if (count1 == count2) {
            statement = true;
        } else {
            statement = false;
        }

        System.out.println(statement);
    }
}